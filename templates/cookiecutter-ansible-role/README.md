Cookiecutter Ansible Role
===

[Cookiecutter](https://github.com/audreyr/cookiecutter) recipe to easily create [ansible roles](http://docs.ansible.com/playbooks_roles.html#roles).

Features
---

1. Follows Ansible [best practices](http://docs.ansible.com/playbooks_best_practices.html)
1. Follows Ansible Galaxy [best practices](https://galaxy.ansible.com/intro#good)
1. Only Creates the necessary files and folders
1. Blazing fast creation, forget about file creation and focus in actions
1. Lint checks ([Ansible-lint](https://github.com/willthames/ansible-lint), [yamllint](https://github.com/adrienverge/yamllint))
1. Test infrastructure already implemented ([Molecule](https://github.com/ansible/molecule)

Template Usage
---

1. Install [cookiecutter](https://cookiecutter.readthedocs.io/en/latest/installation.html#install-cookiecutter): `pip install cookiecutter` (version >= 1.7.0)
1. `git clone git@gitlab.com:anatolek/ansible-roles.git`
1. `cd ansible-roles/roles`
1. `cookiecutter ../templates/cookiecutter-ansible-role`

It will ask you questions about the structure of your role like tasks names, handlers names, and default variables.
You can jump to the next question by entering an empty string.  
Projects are always generated to your current directory.

Example
---

```yaml
full_name [Anatolii Pedchenko]: 
role_name [super_role]:
company_name [MyBigCompany Inc.]:
short_description [This Ansible Role infuses antigravity, you are warned]: 
version [0.0.1]: 0.0.2
min_ansible_version [2.9.0]: 2.9.6
main_docker_image [percygrunwald/docker-centos7-ansible:latest]: 
Should it have handlers? [no]: 
Should it have templates? [no]: 
Should it have files? [no]:
```

Molecule Usage
---

The created role must be in the `gtv/roles/` folder since relative paths are written in the `molecule.yml` file for the linters
```plaintext
../linter_rules/yamllint.yml
../linter_rules/ansiblelint
```

To test a role locally, change directory into the role to test, and execute Molecule as follows
```shell
docker run --rm -it \
    -v "$(dirname $(dirname "${PWD}"))":/tmp/ansible-roles:ro \
    -v /var/run/docker.sock:/var/run/docker.sock \
    --workdir /tmp/ansible-roles/roles/$(basename "${PWD}") \
    quay.io/ansible/molecule:3.0.3 \
    molecule test
```

Contributors
---

[Anatolii Pedchenko](https://gitlab.com/anatolek/) (maintainer)
