#!/usr/bin/env python
from os import remove, path
from shutil import rmtree
from cookiecutter.prompt import read_user_yes_no

optional_folders_list = ['handlers', 'templates', 'files', 'vars']


def configure_folders():
    for folder in optional_folders_list:
        if read_user_yes_no('Should it have {}?'.format(folder), default_value=u'no'):
            try:
                # this file has to be there, git doesn't store empty folders
                remove(path.join(folder, '.empty'))
            except OSError:
                pass
        else:
            rmtree(folder)


if __name__ == '__main__':
    configure_folders()
