Ansible Roles Repository
========================

Main Repository for RedHat Ansible code

Ansible Version
---------------

Current Ansible version is 2.9

Repository Rules
----------------

- All roles should be created with cookiecutter utility
- Versioning rules:
    - Code version contains three positions X.Y.Z
    - **Z** - should be increased for fixes
    - **Y** - should be increased for code update with adding new features
    - **X** - should be increased if update breaks backward compatibility
- `yaml` extension is preferable

CI Description
--------------

To test the role, you must explicitly pass the variable `RUN_MOLECULE_TEST=true` when pushing changes.
And the tests will be run for each changed role separately.
Please note that this feature only works with Gitlab version **12.6.0** and higher.

```shell
git push -o ci.variable="RUN_MOLECULE_TEST=true"
```

Molecule tests are always run for each changed role if there is a merge request for the branch in which you are working.
So, in this case, you don't need to pass the variable since the tests will be run twice.

The configuration file
----------------------

To use settings from the `ansible.cfg` file, you need to run playbooks in the same directory.
