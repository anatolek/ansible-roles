# super_role

{{ cookiecutter.short_description }}

## Requirements

Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here.

## Variables

See [`defaults/main.yaml`](defaults/main.yaml).

## Dependencies

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters
that may need to be set for other roles, or variables that are used from other roles.

## Example Playbook
```
  - hosts: all
    roles:
      - super_role
```
## Testing the role

Important information to know when testing this role

## License

MIT

## Contributors
[Anatolii Pedchenko]() (maintainer)

